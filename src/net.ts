import Bottleneck from 'bottleneck';
import needle from 'needle';
import UserAgent from 'user-agents';

const DEFAULT_BOTTLENECK_OPTIONS: Bottleneck.ConstructorOptions = {
  minTime: 50,
  maxConcurrent: 30,
};

export class RequestProvider {
  private readonly limiter: Bottleneck;

  constructor(
    options: Bottleneck.ConstructorOptions = DEFAULT_BOTTLENECK_OPTIONS
  ) {
    this.limiter = new Bottleneck(options);
  }

  async Get(url: string, options?: needle.NeedleOptions): Promise<any> {
    console.debug(`Performing GET req: ${url}`);
    const response = await this.limiter.schedule(() =>
      needle('get', url, options)
    );
    console.debug(`... GET req finished: ${url}`);
    if (response.statusCode === 200) {
      return response.body;
    } else {
      throw new Error(`Invalid response code: ${response.statusCode}`);
    }
  }
}

const userAgent = new UserAgent({ deviceCategory: 'desktop' });
const DEFAULT_NEEDLE_OPTIONS: needle.NeedleOptions = {
  user_agent: userAgent.toString(),
};

needle.defaults(DEFAULT_NEEDLE_OPTIONS);

export const singleton = new RequestProvider();
