const CATEGORY_URL = 'https://clutch.co/web-developers';
import { singleton } from './net';
import ClutchParser from './ClutchParser';
import fs from 'fs';
import path from 'path';

const OUTPUT_DIRECTORY = path.resolve('.', 'output');

const init = async () => {
  return new Promise((resolve, reject) => {
    fs.mkdir(OUTPUT_DIRECTORY, { recursive: true }, err => {
      if (err) {
        return reject(err);
      } else {
        resolve();
      }
    });
  });
};

const saveCategory = async (
  categoryUrl: string,
  data: Array<any>
): Promise<void> => {
  const categoryNameSplitterPos = categoryUrl.lastIndexOf('/');
  const categoryFileName =
    categoryUrl.slice(categoryNameSplitterPos + 1) + '.json';
  const fileName = path.join(OUTPUT_DIRECTORY, categoryFileName);
  const content = JSON.stringify(data, null, '\t');
  console.log(`Saving ${data.length} items`);
  console.log(`Saving file: ${fileName}`);
  return new Promise((resolve, reject) => {
    fs.writeFile(fileName, content, err => {
      if (err) {
        return reject(err);
      } else {
        return resolve();
      }
    });
  });
};

const processPage = async (categoryUrl: string, pageNumber: number) => {
  const finalUrl = pageNumber
    ? `${categoryUrl}?page=${pageNumber}`
    : categoryUrl;
  const htmlContent = await singleton.Get(finalUrl);
  const parser = new ClutchParser();
  return parser.parse(htmlContent);
};

const main = async (categoryUrl: string) => {
  await init();
  const htmlContent = await singleton.Get(categoryUrl);
  const parser = new ClutchParser();
  const pageCount = parser.getPageCountInCategory(htmlContent);
  const promises = new Array(pageCount);
  // for (let i = 0; i <= 0; ++i) {
  for (let i = 0; i <= pageCount; ++i) {
    promises[i] = processPage(categoryUrl, i);
  }
  const data = await Promise.all(promises);
  const flatData = data.flat();
  return saveCategory(categoryUrl, flatData);
};

main(CATEGORY_URL).catch(e => console.error(e));
