import * as cheerio from 'cheerio';

type ListItem = {
  title?: string;
  value?: string;
};

type Organisation = {
  companyName?: string;
  profileUrl?: string;
  tagline?: string;
  webSiteUrl?: string;
  description?: string;
  listItems: Array<ListItem>;
  chartItems: Array<ChartItem>;
  locality?: string;
  region?: string;
  founders: Array<string>;
};

type ChartItem = {
  name?: string;
  value?: number;
};

export default class ClutchParser {
  getPageCountInCategory(html: string): number {
    const $ = cheerio.load(html);
    const element = $('.pager-last a');
    if (!element) {
      console.warn(`Can't get page count element`);
      return -1;
    }
    const href = element.attr('href');
    if (!href) {
      console.warn(`Can't get href for count element`);
      return -1;
    }
    const countStr = href?.split('page=')[1];
    if (!countStr) {
      console.warn(`Invalid count format: ${countStr}`);
      return -1;
    }
    return Number.parseInt(countStr, 10);
  }

  parse(html: string): Array<Organisation> {
    const $ = cheerio.load(html);
    const cards = $('li.provider-row');
    const cardsCount = cards.length;
    const arrResult: Array<Organisation> = [];
    for (let i = 0; i < cardsCount; ++i) {
      const card = cards[i];
      const result = ClutchParser.parseCard($, card);
      if (result) {
        arrResult.push(result);
      }
    }
    return arrResult;
  }

  private static parseCard(
    $: CheerioStatic,
    card: CheerioElement
  ): Organisation | undefined {
    const result: Organisation = {
      listItems: new Array<ListItem>(),
      chartItems: new Array<ChartItem>(),
      founders: new Array<string>(),
    };
    result.companyName = $('.company-name', card).text().trim();
    if (!result.companyName) {
      console.warn(`No company name. Must be ads. Skipping`);
      return;
    }
    result.profileUrl = $('.company-name a', card).attr('href');
    result.tagline = $('.tagline', card).text().trim();
    result.webSiteUrl = $('.website-link a', card).attr('href');
    result.description = $(
      '.provider-info__description .blockquote-in-module',
      card
    )
      .text()
      .trim();

    let locality = $('.locality', card).text().trim();
    if (locality && locality[locality.length - 1] === ',') {
      locality = locality.slice(0, locality.length - 1);
    }
    result.locality = locality;
    result.region = $('.region', card).text().trim();

    ClutchParser.parseListItems($, card, result);
    ClutchParser.parseFounders($, card, result);
    ClutchParser.parseChartItems($, card, result);
    return result;
  }

  private static parseFounders(
    $: CheerioStatic,
    card: CheerioElement,
    result: Organisation
  ) {
    const foundersElements = $('.small-list li', card);
    const foundersCount = foundersElements.length;
    for (let i = 0; i < foundersCount; ++i) {
      const foundersElement = foundersElements[i];
      result.founders.push($(foundersElement).text().trim());
    }
  }

  private static parseChartItems(
    $: CheerioStatic,
    card: CheerioElement,
    result: Organisation
  ) {
    const chartItems = $('.chartAreaContainer .grid', card);
    const chartItemsCount = chartItems.length;
    for (let k = 0; k < chartItemsCount; ++k) {
      const chartItem = chartItems[k];
      const dataContent = $(chartItem).attr('data-content');
      if (!dataContent) {
        continue;
      }
      const jqC = cheerio.load(dataContent);
      const chartItemObj: ChartItem = {};
      chartItemObj.name = jqC('b').text().trim();
      chartItemObj.value = Number.parseInt(jqC('i').text().trim(), 10);
      result.chartItems.push(chartItemObj);
    }
  }

  private static parseListItems(
    $: CheerioStatic,
    card: CheerioElement,
    result: Organisation
  ) {
    const listItems = $('.module-list .list-item', card);
    const listItemsCount = listItems.length;
    for (let j = 0; j < listItemsCount; ++j) {
      const listItem = listItems[j];
      const listItemObj: ListItem = {
        title: $('i', listItem).attr('title'),
        value: $(listItem).text().trim(),
      };
      result.listItems.push(listItemObj);
    }
  }
}
